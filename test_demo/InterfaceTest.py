# coding: 'utf-8'
import unittest
import urllib3
import requests

# create需要返回的是response_create.json()，命名为response_create_json
# list需要返回的是json_list['status']
# delete需要返回的是json_list，即信息列表

def Create(url_create):

        #  create
        data = {"puid": 25001,
                "did": 189,
                "ctype": 1,
                "sctype": 1,
                "title": "科室名称",
                "content": "胃科",
                "target": "解决胃的相关问题",
                "target_type": 1,
                # "expire_time": "2020-08-03 10:10:10",
                "category": "复诊",
                "app_id": "5dc8b9d9b60c4850b7fbb171"
                }

        response_create = requests.post(url_create, data=data, verify=False)
        json_create = response_create.json()

        if json_create['status'] != 0:
                return False

        return json_create

def List(url_list, params):

        #   list
        response_list = requests.get(url_list, params)
        json_list = response_list.json()
        return json_list

def Delete(url_delete, params, verify, url_list, params1):
        response_delete = requests.post(url_delete, params, verify)
        json_delete = response_delete.json()

        if json_delete['status'] != 0:
                print(json_delete['message'])

        json_list = List(url_list, params1)

        for key in list(List(url_list, params1).keys()):
                del json_list[key]

        return json_list

def clear_data(url_list, params, url_delete):
    response = requests.get(url_list, params)
    json_list = response.json()
    if json_list['status'] != 0:
        return False

    for json_data in json_list:
        #   删除数据
        if json_list['total']:
            params_delete = {"cards": response.content['result'][0]["id"]}
            Delete(url_delete, params_delete, False, url_list, params)

class loopTest(unittest.TestCase):

    url_create = "https://zsys-test.zuoshouyisheng.com/dm_api/internal/task_cards/create"
    url_list = "https://zsys-test.zuoshouyisheng.com/dm_api/task_cards/list"
    url_delete = "https://zsys-test.zuoshouyisheng.com/dm_api/internal/task_cards/delete"
    params = {"auth_code": "XtDMOGVQwTuVFzrwcOPUbhubJexVyJyD", "platform": "", "page": "1"}

    @classmethod
    def setUpClass(cls) -> None:
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def setUp(self) -> None:

        # 提前确保list里面无数据
        #   拉取数据   删除数据
        clear_data(self.url_list, self.params, self.url_delete)

    def tearDown(self) -> None:
        pass

    def test_Create(self):

        #   create
        data = {"puid": 25001,
                "did": 189,
                "ctype": 1,
                "sctype": 1,
                "title": "科室名称",
                "content": "胃科",
                "target": "解决胃的相关问题",
                "target_type": 1,
                # "expire_time": "2020-08-03 10:10:10",
                "category": "复诊",
                "app_id": "5dc8b9d9b60c4850b7fbb171"
                }

        response_create = requests.post(self.url_create, data=data, verify=False)
        json_create = response_create.json()

        if json_create['status'] != 0:
            return False

        response_create_json = response_create.json()

        #   list
        json_list = List(self.url_list, self.params)
        if json_list['status'] != 0:
            return False

        self.assertEqual(json_list['total'], 1)
        self.assertEqual(response_create_json['card'], json_list['result'][0]['id'])

        #   delete
        params_delete = {"cards": response_create_json['card']}
        json_list_delete = Delete(self.url_delete, params_delete, False, self.url_list, self.params)
        self.assertEqual(json_list_delete, {})

    def test_Finish(self):

        # create
        response_create_json = Create(self.url_create)

        #   finish
        url_finish = "https://zsys-test.zuoshouyisheng.com/dm_api/internal/task_cards/finish"
        params_finish = {"card": response_create_json['card']}

        response_finish = requests.post(url_finish, data=params_finish, verify=False)
        json_finish = response_finish.json()

        if json_finish['status'] != 0:
            return False

        #   list
        json_list = List(self.url_list, self.params)

        if json_list['status'] != 0:
            return False

        self.assertEqual(json_list['total'], 1)
        self.assertEqual(response_create_json['card'], json_list['result'][0]['id'])

        #   delete
        params_delete = {"cards": response_create_json['card']}
        json_list_delete = Delete(self.url_delete, params_delete, False, self.url_list, self.params)
        self.assertEqual(json_list_delete, {})

        return True

if __name__ == '__main__':
    unittest.main()